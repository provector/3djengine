package provector.ie.jengine.rendering;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL32.*;


import provector.ie.jengine.rendering.entities.Vector3f;

public class RenderUtil {

	public static void clearScreen() {
		//TODO: Stencil Buffer
		
		//clear screen buffers:
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	
	public static void setTextures(boolean enabled) {
		if(enabled) {
			glEnable(GL_TEXTURE_2D);
		}else {
			glDisable(GL_TEXTURE_2D);
		}
	}
	
	public static void initGraphics() {
		//set all pixels to this color //black 0,0,0,0
		glClearColor(0.0f,0.0f,0.0f,0.0f); //TODO: Possible color converter?
		
		//call only visible front facing
		glFrontFace(GL_CW); //every face drawn will be clockwise
		glCullFace(GL_BACK); //dont draw back
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST); //remember z history
		
		glEnable(GL_DEPTH_CLAMP);
		glEnable(GL_TEXTURE_2D);
	}
	
	public static void setClearColor(Vector3f color) {
		glClearColor(color.getX(),color.getY(),color.getZ(),1.0f);
	}
	
	public static String getOpenGLVersion() {
		return glGetString(GL_VERSION);
	}

	public static void unbindTextures() {
		glBindTexture(GL_TEXTURE_2D,0);
	}
}
