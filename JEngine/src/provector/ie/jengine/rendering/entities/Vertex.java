package provector.ie.jengine.rendering.entities;

public class Vertex {

	public static final int SIZE = 8; //5 < 0.23dev
	
	private Vector3f pos;
	private Vector3f normal;
	private Vector2f textureCoord;
		
	public Vertex(Vector3f pos) {
		this(pos, new Vector2f(0,0));
	}
	
	public Vertex(Vector3f pos,Vector2f texCoord) {
		this(pos,texCoord,new Vector3f(0,0,0));
	}
	
	public Vertex(Vector3f pos,Vector2f textureCoord,Vector3f normal) {
		this.pos = pos;
		this.textureCoord = textureCoord;
		this.normal = normal;
	}

	public Vector3f getPos() {
		return pos;
	}

	public void setPos(Vector3f pos) {
		this.pos = pos;
	}

	public Vector2f getTextureCoord() {
		return textureCoord;
	}

	public void setTextureCoord(Vector2f textureCoord) {
		this.textureCoord = textureCoord;
	}

	public Vector3f getNormal() {
		return normal;
	}

	public void setNormal(Vector3f normal) {
		this.normal = normal;
	}
	
	@Override
	public String toString() {
		return new String("\n{\n\t(vec3)pos="+pos.getX()+","+pos.getY()+","+pos.getZ()+"\n"
						 +"\t(vec3)normal="+normal.getX()+","+normal.getY()+","+normal.getZ()+"\n"
						 +"\t(vec2)texCoord="+textureCoord.getX()+","+textureCoord.getY()+"\n}\n");
	}
	
}
