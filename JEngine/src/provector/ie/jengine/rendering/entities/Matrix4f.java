package provector.ie.jengine.rendering.entities;

public class Matrix4f {

	private float[][] m;
	
	public Matrix4f() {
		this.m =  new float[4][4];
	}
	
	public Matrix4f initIdentity() {
		this.m = new float[][] {
			{1,0,0,0},
			{0,1,0,0},
			{0,0,1,0},
			{0,0,0,1}
		};
		return this;
	}
	
	public Matrix4f initTranslation(float x,float y,float z) {
		
		this.m = new float[][] {
			{1,0,0,x},
			{0,1,0,y},
			{0,0,1,z},
			{0,0,0,1}
		};		
		return this;
	}
	
	public Matrix4f initRotation(float x,float y,float z) {
		
		Matrix4f rx = new Matrix4f();
		Matrix4f ry = new Matrix4f();
		Matrix4f rz = new Matrix4f();
		
		x = (float) Math.toRadians(x);
		y = (float) Math.toRadians(y);
		z = (float) Math.toRadians(z);
		
		float cosZ = (float) Math.cos(z);
		float sinZ = (float) Math.sin(z);
		float cosY = (float) Math.cos(y);
		float sinY = (float) Math.sin(y);
		float cosX = (float) Math.cos(x);
		float sinX = (float) Math.sin(x);
		
		rz.m = new float[][] {
			{cosZ,-sinZ,0,0},
			{sinZ, cosZ,0,0},
			{	0, 	  0,1,0},
			{	0,	  0,0,1}
		};
		
		rx.m = new float[][] {
			{1,	  0,    0,0},
			{0,cosX,-sinX,0},
			{0,sinX, cosX,0},
			{0,	  0,	0,1}
		};
		
		ry.m = new float[][] {
			{cosY,0,-sinY,0},
			{	0,1,	0,0},
			{sinY,0, cosY,0},
			{	0,0,	0,1}		
		};
		
		this.m = rz.mul(ry.mul(rx)).getM();
		return this;
	}
	
	public Matrix4f initScale(float x,float y,float z) {
		
		this.m = new float[][] {
			{x,0,0,0},
			{0,y,0,0},
			{0,0,z,0},
			{0,0,0,1}
		};
		
		return this;
	}
	
	public Matrix4f initProjection(float fov,float width,float height,float zNear,float zFar ) {
		
		float ar = width/height; //aspect ratio
		float tanHalfFOV = (float) Math.atan(Math.toRadians(fov/2));
		float zRange = zNear - zFar;
		
		this.m = new float[][] {
			{1.0f/(tanHalfFOV*ar),				0,					 0,					 0},
			{					0,1.0f/tanHalfFOV,					 0,					 0},
			{					0,				0,(-zNear-zFar)/zRange,2*zFar*zNear/zRange},
			{					0,				0,					 1,					 0}
		};
		
		return this;
	}
	
	public Matrix4f initCamera(Vector3f forward,Vector3f up) {
		
		Vector3f f = forward;
		f.normalize();
		
		//get right vector
		Vector3f r = up;
		r.normalize();
		r = r.cross(f);
		
		//recalculate up
		Vector3f u = f.cross(r);
		
		this.m = new float[][] {
			{r.getX(),r.getY(),r.getZ(),0},
			{u.getX(),u.getY(),u.getZ(),0},
			{f.getX(),f.getY(),f.getZ(),0},
			{0		 ,		 0		 ,0,1}
		};
		
		return this;		
	}
	
	public Matrix4f mul(Matrix4f r) {
		
		Matrix4f res = new Matrix4f();
		for(int i=0;i<4;i++) 
		{
			for(int j=0;j<4;j++) 
			{
				res.set(i,j,m[i][0]*r.get(0,j)+
						  m[i][1]*r.get(1,j)+
						  m[i][2]*r.get(2,j)+
						  m[i][3]*r.get(3,j));
			}
		}
		return res;
	}

	public float[][] getM() {
		float[][] copy = m;
		return copy;
	}
	
	public float get(int x,int y) {
		return this.m[x][y];
	}

	public void setM(float[][] m) {
		this.m = m;
	}
	
	public void set(int x,int y,float value) {
		this.m[x][y] = value;
	}
	
	
}
