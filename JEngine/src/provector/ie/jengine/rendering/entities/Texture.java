package provector.ie.jengine.rendering.entities;

import static org.lwjgl.opengl.GL11.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.newdawn.slick.opengl.TextureLoader;

public class Texture {
	
	private int id;
	
	public Texture(String fileName) {
		this(loadTexture(fileName));
	}
	
	public Texture(int id) {
		this.id = id;
	}
	
	public void bind() {
		glBindTexture(GL_TEXTURE_2D,id);
	}
	
	public int getId() {
		return this.id;
	}
	
	private static int loadTexture(String fileName) {
		
		String[] splitArray = fileName.split("\\.");
		String ext = splitArray[splitArray.length-1];
				
		try {
			int id = TextureLoader.getTexture(ext,new FileInputStream(new File("res/textures/"+fileName))).getTextureID();
			return id;
			
		}catch(IOException io) {
			System.out.println("Error loading texture: "+io.getMessage());
			io.printStackTrace();
			return 0;
		}
	}

}
