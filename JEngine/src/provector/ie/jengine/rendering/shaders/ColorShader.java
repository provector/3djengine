package provector.ie.jengine.rendering.shaders;

import provector.ie.jengine.rendering.entities.Matrix4f;
import provector.ie.jengine.rendering.entities.Vector3f;

public class ColorShader extends Shader {

	private static ColorShader instance;// = new ColorShader();
	
	public static ColorShader getInstance() {
		if(instance==null) {
			instance = new ColorShader();
		}
		return instance;
	}

	private ColorShader() {
		
		super();
		addVertexShaderFromFile("basicColor_VS.glsl");
		addFragmentShaderFromFile("basicColor_FS.glsl");
		compileShader();
		
		addUniform("inColor");
		addUniform("transform");
	}
	
	public void updateUniforms(Matrix4f worldMatrix,Matrix4f projectedMatrix,Vector3f color) {
		
		setUniform("inColor",color);
		setUniform("transform",projectedMatrix);
		
	}
}
