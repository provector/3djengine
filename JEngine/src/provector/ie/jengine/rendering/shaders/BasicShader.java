package provector.ie.jengine.rendering.shaders;

import provector.ie.jengine.rendering.RenderUtil;
import provector.ie.jengine.rendering.entities.Material;
import provector.ie.jengine.rendering.entities.Matrix4f;

public class BasicShader extends Shader {

	private static BasicShader instance;// = new BasicShader();
	private int texturePresent;
	
	public static BasicShader getInstance() {
		if(instance==null) {
			instance = new BasicShader();
		}
		return instance;
	}
	
	
	private BasicShader() {
		super();
		
		addVertexShaderFromFile("basicTexture_VS.glsl");
		addFragmentShaderFromFile("basicTexture_FS.glsl");
		compileShader();
		
		addUniform("transform");
		addUniform("color");
		addUniform("texturePresent");
	}
	
	@Override
	public void updateUniforms(Matrix4f worldMatrix,Matrix4f projectedMatrix,Material material) {
		
		if(material.getTexture()!=null) {
			texturePresent = 1;
			material.getTexture().bind();
		}else {
			texturePresent = 0;
			RenderUtil.unbindTextures();
		}
		setUniform("transform",projectedMatrix);
		setUniform("color",material.getColor());
		setUniformi("texturePresent",texturePresent);
	}
}
