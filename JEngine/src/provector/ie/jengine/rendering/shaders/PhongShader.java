package provector.ie.jengine.rendering.shaders;


import provector.ie.jengine.rendering.Transform;
import provector.ie.jengine.rendering.entities.Material;
import provector.ie.jengine.rendering.entities.Matrix4f;
import provector.ie.jengine.rendering.entities.Vector3f;
import provector.ie.jengine.rendering.entities.lights.BaseLight;
import provector.ie.jengine.rendering.entities.lights.DirectionalLight;
import provector.ie.jengine.rendering.entities.lights.PointLight;
import provector.ie.jengine.rendering.entities.lights.SpotLight;

public class PhongShader extends Shader{
	
	private static final int MAX_POINT_LIGHTS = 4; //keep in sync with FS!
	private static final int MAX_SPOT_LIGHTS = 4;
	
	private static PhongShader instance;// = new PhongShader();
	
	public static PhongShader getInstance() {
		if(instance==null) {
			instance = new PhongShader();
		}
		return instance;
	}
	
	/*
	 * This code (new instance) actually loads all the created shaders (for each one)
	 * into gpu memory. "Re-think" this singleton approach...
	 */
		
	private static Vector3f ambientLight = new Vector3f(1.0f,1.0f,1.0f);
	private static DirectionalLight directionalLight = 
			new DirectionalLight(
				new BaseLight(
					new Vector3f(1,1,1),0),
				new Vector3f(0,0,0)
			);
	private static PointLight[] pointLights = new PointLight[] {};
	private static SpotLight[] spotLights = new SpotLight[] {};
	
	
	
	private PhongShader() {
		super();
		
		addVertexShaderFromFile("PhongLight_VS.glsl");
		addFragmentShaderFromFile("PhongLight_FS.glsl");
		compileShader();
		
		addUniform("transform");
		addUniform("transformProjected");
		addUniform("baseColor");
		addUniform("ambientLight");	
		addUniform("specularIntensity");
		addUniform("specularPower");
		addUniform("eyePos");
		
		addUniform("directionalLight.base.color");
		addUniform("directionalLight.base.intensity");
		addUniform("directionalLight.direction");
		
		for(int i=0;i<MAX_POINT_LIGHTS;i++) {
			addUniform("pointLights["+i+"].base.color");
			addUniform("pointLights["+i+"].base.intensity");
			addUniform("pointLights["+i+"].atten.constant");
			addUniform("pointLights["+i+"].atten.linear");
			addUniform("pointLights["+i+"].atten.exponent");
			addUniform("pointLights["+i+"].position");		
			addUniform("pointLights["+i+"].range");
		}
		
		for(int i=0;i<MAX_SPOT_LIGHTS;i++) {
			addUniform("spotLights["+i+"].pointLight.base.color");
			addUniform("spotLights["+i+"].pointLight.base.intensity");
			addUniform("spotLights["+i+"].pointLight.atten.constant");
			addUniform("spotLights["+i+"].pointLight.atten.linear");
			addUniform("spotLights["+i+"].pointLight.atten.exponent");
			addUniform("spotLights["+i+"].pointLight.position");		
			addUniform("spotLights["+i+"].pointLight.range");
			addUniform("spotLights["+i+"].direction");
			addUniform("spotLights["+i+"].cutoff");
		}
		
	}
	
	public void updateUniforms(Matrix4f worldMatrix,Matrix4f projectedMatrix,Material material) {
		setUniform("transform",worldMatrix);
		setUniform("transformProjected",projectedMatrix);
		setUniform("baseColor",material.getColor());
		
		setUniform("ambientLight",ambientLight);
		setUniform("directionalLight",directionalLight);
		
		for(int i=0;i<pointLights.length;i++) {
			setUniform("pointLights["+i+"]",pointLights[i]);
		}
		
		for(int i=0;i<spotLights.length;i++) {
			setUniform("spotLights["+i+"]",spotLights[i]);
		}
		
		setUniformf("specularIntensity",material.getSpecularIntensity());
		setUniformf("specularPower",material.getSpecularPower());
		
		setUniform("eyePos",Transform.getCamera().getPosition());
		
	}
	
	public void setUniform(String uniformName,BaseLight baseLight) {
		setUniform(uniformName+".color",baseLight.getColor());
		setUniformf(uniformName+".intensity",baseLight.getIntensity());
	}
	
	public void setUniform(String uniformName,DirectionalLight directionalLight) {
		setUniform(uniformName+".base",directionalLight.getBase());
		setUniform(uniformName+".direction",directionalLight.getDirection());
	}
	
	public void setUniform(String uniformName,PointLight pointLight) {
		setUniform(uniformName+".base",pointLight.getBaseLight());
		setUniformf(uniformName+".atten.constant",pointLight.getAtten().getConstant());
		setUniformf(uniformName+".atten.linear",pointLight.getAtten().getLinear());
		setUniformf(uniformName+".atten.exponent",pointLight.getAtten().getExponent());
		setUniform(uniformName+".position",pointLight.getPosition());
		setUniformf(uniformName+".range",pointLight.getRange());
	}
	
	public void setUniform(String uniformName,SpotLight spotLight) {
		setUniform(uniformName+".pointLight",spotLight.getPointLight());
		setUniform(uniformName+".direction",spotLight.getDirection());
		setUniformf(uniformName+".cutoff",spotLight.getCutoff());
		

	}

	public Vector3f getAmbientLight() {
		return ambientLight;
	}

	public static void setAmbientLight(Vector3f ambient_Light) {
		ambientLight = ambient_Light;
	}

	public static DirectionalLight getDirectionalLight() {
		return directionalLight;
	}

	public static void setDirectionalLight(DirectionalLight directionalLight) {
		PhongShader.directionalLight = directionalLight;
	}

	//TODO: getters for those two?
	
	public static void setPointLight(PointLight[] pointLights_) {
		if(pointLights.length>MAX_POINT_LIGHTS) {
			System.out.println("Error: To many point lights: "+pointLights.length+" over "+MAX_POINT_LIGHTS);
			new Exception().printStackTrace();
			System.exit(1);
		}
		pointLights = pointLights_;
	}
	
	public static void setSpotLight(SpotLight[] spotLights_) {
		if(spotLights.length>MAX_SPOT_LIGHTS) {
			System.out.println("Error: To many spot lights: "+spotLights.length+" over "+MAX_SPOT_LIGHTS);
			new Exception().printStackTrace();
			System.exit(1);
		}
		spotLights = spotLights_;
	}

}
