package provector.ie.jengine;

import provector.ie.jengine.components.Input;
import provector.ie.jengine.components.Time;
import provector.ie.jengine.components.Util;
import provector.ie.jengine.components.Window;
import provector.ie.jengine.rendering.RenderUtil;

public class MainComponent {

	public static final String _VER = "0.31";
	
	public static final boolean SHOW_FPS = true;
	public static final int WIDTH = 1600;
	public static final int HEIGHT = 900;
	public static final String TITLE = "3DJEngine ver "+_VER;
	
	public static final double FRAME_CAP = 75.0;
	
	private boolean isRunning;
	private Game game;
	
	public MainComponent() {
		System.out.println("Initiating "+TITLE);
		System.out.println(RenderUtil.getOpenGLVersion());
		
		RenderUtil.initGraphics();
		isRunning = false;
		game = new Game();
	}
	
	public void start() {
		
		if(isRunning) {
			System.out.println("Instance already running!");
			return;
		}
		run();
	}
	
	public void stop() {
		if(!isRunning) {
			System.out.println("Instance not found!");
			return;
		}
		isRunning = false;
	}
	
	private void run() 
	{
		isRunning = true;
		
		int frames = 0;
		long frameCounter = 0;
		
		final double frameTime = 1.0 / FRAME_CAP;
		
		long lastTime = Time.getTime();
		double unprocessedTime = 0;
		
		while(isRunning) {
			
			boolean render = false;
			
			long startTime = Time.getTime();
			long passedTime = startTime - lastTime;
			lastTime = startTime;
			
			//that gives how much time is passed in double form
			unprocessedTime += passedTime / (double)Time.SECOND;
			frameCounter += passedTime;
			
			while(unprocessedTime > frameTime) 
			{
				render = true;
				
				unprocessedTime -= frameTime;
				
				if(Window.isCloseRequested()) 
				{
					stop();
				}
				
				Time.setDelta(frameTime);
				
				
				//get Input
				game.input();
				Input.update();
				game.update();	
				
				//Show Fps
				if(frameCounter >= Time.SECOND)
				{
					if(SHOW_FPS) System.out.println(frames);
					frames = 0;
					frameCounter = 0;
				}
			}
			if(render) {
				render();
				frames++;
			}else {
				Util.sleep(1);
			}
			
		}
		cleanUp();
		
	}
	
	private void render() {
		
		RenderUtil.clearScreen();
		game.render();
		Window.render();
	}
		
	private void cleanUp() {
		Window.dispose();
	}
	
	public static void main(String[] args) {
		Window.createWindow(WIDTH, HEIGHT, TITLE);
		
		MainComponent game = new MainComponent();
		game.start();
	}
}
