package provector.ie.jengine.components;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;

import provector.ie.jengine.rendering.entities.Matrix4f;
import provector.ie.jengine.rendering.entities.Vertex;

public class Util {

	
	private static FloatBuffer createFloatBuffer(int size) {
		return BufferUtils.createFloatBuffer(size);
	}
	
	//TODO: Important, modify this method any time with add more data to vertices
	
	public static FloatBuffer createFlippedBuffer(Vertex[] vertices) 
	{
		FloatBuffer buffer = createFloatBuffer(vertices.length*Vertex.SIZE);
		for(int i=0;i<vertices.length;i++) {
			buffer.put(vertices[i].getPos().getX());
			buffer.put(vertices[i].getPos().getY());
			buffer.put(vertices[i].getPos().getZ());
			buffer.put(vertices[i].getTextureCoord().getX());
			buffer.put(vertices[i].getTextureCoord().getY());
			buffer.put(vertices[i].getNormal().getX());
			buffer.put(vertices[i].getNormal().getY());
			buffer.put(vertices[i].getNormal().getZ());
		}
		
		buffer.flip();
		
		return buffer;
	}
	
	public static FloatBuffer createFlippedBuffer(Matrix4f value) {
		FloatBuffer buffer = createFloatBuffer(4*4);
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				buffer.put(value.get(i,j));
			}
		}
		buffer.flip();
		return buffer;
	}
	
	public static IntBuffer createIntBuffer(int size) {
		return BufferUtils.createIntBuffer(size);
	}
	
	public static IntBuffer createFlippedBuffer(int[] values) {
		IntBuffer buffer = createIntBuffer(values.length);
		buffer.put(values);
		buffer.flip();
		return buffer;
	}
	
	public static void sleep(long ms) {
		try {
			Thread.sleep(ms);
		}catch(InterruptedException e) {
			System.out.println("Thread access violation: "+e.getMessage());
		}
	}
	
	public static int[] toIntArray(Integer[] data) {
		int[] result = new int[data.length];
		for(int i=0;i<data.length;i++) {
			result[i] = data[i].intValue();
		}
		return result;
	}
}
