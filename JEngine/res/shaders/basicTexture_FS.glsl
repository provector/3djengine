#version 330

in vec2 texCoord0;

uniform vec3 color;
uniform sampler2D sampler;
uniform int texturePresent;

out vec4 fragColor;

void main()
{
	vec4 texture = texture2D(sampler,texCoord0.xy);

	if(texturePresent == 0){
		fragColor = vec4(color,1.0);
	}else{
		fragColor = texture * vec4(color,1.0);
	}
}
